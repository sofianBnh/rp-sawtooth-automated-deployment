Test Deployment of Sawtooth DLT
===============================


Each node of the system has:

- validator container
- poet-engine container
- poet-validator-registry-tp container
- rest-api container
- intkey-tp container

And the boot node start a shell with it.

----

To start the boot node:

```bash
docker-compose -f docker-compose-boot-node.yaml up
```

---

To generate a node:

```bash
bash gen-node-compose.sh node_id
```

Example:

```bash
bash gen-node-compose.yaml 1
```
This will generate a docker-compose file with valdiator-1, rest-api-1, ...

---

To start the node with its containers:

```bash
docker-compose -f docker-compose-node-node_id.yaml up
```

Example:


```bash
docker-compose -f docker-compose-node-1.yaml up
```

This will start valdiator-1, rest-api-1, ...


import zmq
from sawtooth_sdk.messaging.stream import Stream
from sawtooth_sdk.protobuf.events_pb2 import EventSubscription, EventList
from sawtooth_sdk.protobuf.client_event_pb2 import ClientEventsSubscribeRequest, ClientEventsSubscribeResponse
from sawtooth_sdk.protobuf.validator_pb2 import Message

done_sub = EventSubscription(event_type='do-nothing-done')
_stream = Stream('tcp://boot-node-validator:4004')
request = ClientEventsSubscribeRequest(
    subscriptions=[done_sub])

response_future = _stream.send(
    Message.CLIENT_EVENTS_SUBSCRIBE_REQUEST,
    request.SerializeToString())
sub_response = ClientEventsSubscribeResponse()
sub_response.ParseFromString(response_future.result().content)

print('The status ' + sub_response.status )
if sub_response.status != ClientEventsSubscribeResponse.OK:
    print('Wrong status')

_is_active = True

while _is_active:
    message_future = _stream.receive()

    event_list = EventList()
    event_list.ParseFromString(message_future.result().content)

    for event in event_list.events:
        print('event received')



# https://github.com/hyperledger/sawtooth-marketplace/blob/master/ledger_sync/marketplace_ledger_sync/main.py